package com.mobilecomputing.conceptorecyclerviewtablayout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.*
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_recycler.view.*

class MainActivity : AppCompatActivity() {
    lateinit var tabLayout: TabLayout
    lateinit var recyclerView:RecyclerView
    lateinit var viewAdapter: RecyclerView.Adapter<*>
    lateinit var viewManager: RecyclerView.LayoutManager
    val myDataset:ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.myDataset.add("1")
        this.myDataset.add("2")
        this.myDataset.add("3")
        this.myDataset.add("4")
        this.myDataset.add("5")
        this.myDataset.add("6")
        this.myDataset.add("7")
        this.myDataset.add("8")
        this.myDataset.add("9")
        this.myDataset.add("10")

        this.tabLayout = tabLayoutBlock
        this.tabLayout.tabMode = TabLayout.MODE_SCROLLABLE

        this.myDataset.forEach {
            item->
            this.tabLayout.addTab(tabLayout.newTab().setText(item))
        }

        viewManager = LinearLayoutManager(this)

        viewAdapter = RecyclerViewBlockAdapter(myDataset)

        this.recyclerView = recyclerViewBlock

        this.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(recyclerView)

        this.recyclerView.addOnScrollListener(object: RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {



            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    var snapPosition = snapHelper.getSnapPosition(recyclerView)

                    if(snapPosition!=null){
                        tabLayout.setScrollPosition(snapPosition,0f,false)
                    }
                }
            }
        })



        this.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                val positionTab = tab.position
                recyclerView.smoothScrollToPosition(positionTab)
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
    }
}

fun SnapHelper.getSnapPosition(recyclerView: RecyclerView): Int {
    val layoutManager = recyclerView.layoutManager ?: return RecyclerView.NO_POSITION
    val snapView = findSnapView(layoutManager) ?: return RecyclerView.NO_POSITION
    return layoutManager.getPosition(snapView)
}


class RecyclerViewBlockAdapter(private val myDataset: ArrayList<String>):RecyclerView.Adapter<RecyclerViewBlockAdapter.MyViewHolder>(){


    class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val atmText:TextView
        init {
            atmText = itemView.TextViewIndice as TextView
        }
    }


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,viewType: Int):MyViewHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_recycler, parent, false)
        return MyViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.atmText.setText(myDataset[position])
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myDataset.size
}
